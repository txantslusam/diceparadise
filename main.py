import cv2
import numpy as np
import math
import os

min_threshold =     80
max_threshold =     240
min_area =          30
min_circularity =   .5
min_inertia_ratio = .8

cap = cv2.VideoCapture(0)
color = 0


class Point:

    def __init__(self, x, y, diameter):
        self.x = x
        self.y = y
        self.diameter = diameter


def sort_by_dist(curr_point, kpt):
    if (len(kpt) > 1):
        for dot in kpt:
            dot.dist = math.sqrt((curr_point.x - dot.x) ** 2 + (curr_point.y - dot.y) ** 2)
        kpt.sort(key=lambda k: k.dist)
    return kpt


def counter(img_with_keypoints, keypoints):
    org = []
    for j in keypoints:
        org.append(Point(j.pt[0], j.pt[1], j.size))
    to_classify = org[:]

    iter = 0
    while (len(to_classify) != 0 and len(org) > 0 and iter < 1000):
        curr_point = to_classify.pop()
        out = 1.8 * curr_point.diameter
        inside = 0.2 * curr_point.diameter
        org = sort_by_dist(curr_point, org)

        # kpt[1] because kpt[0].dist is equal to 0 (distance between the same point)
        # 1len > 2x (1)

        if (len(org) == 1 or org[1].dist > 2.5 * (out + inside)):
            img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                            (255, 0, 0), 1)
            cv2.putText(img=img_with_keypoints, text=str(1), org=(int(curr_point.x), int(curr_point.y)),
                        fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

        # 1len = 2x (2)
        elif (len(org) > 1 and 2 * (out - inside) < org[1].dist < 2 * (out + inside)):
            img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                            (255, 255, 0),
                                            1)
            to_classify = sort_by_dist(curr_point, to_classify)
            if (len(to_classify) == 0):
                break
            next_dot = to_classify.pop(0)
            img_with_keypoints = cv2.circle(img_with_keypoints, (int(next_dot.x), int(next_dot.y)), int(out),
                                            (255, 255, 0),
                                            1)
            cv2.putText(img=img_with_keypoints, text=str(2), org=(int(curr_point.x), int(curr_point.y)),
                        fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

        # 1len = x, 2len = x (central dot of 3 or 5)
        elif (len(org) > 2 and (out - inside) < org[1].dist < (out + inside) and (out - inside) < org[2].dist < (
                out + inside)):
            # 3len = x (5)
            if (len(org) > 4 and (out - inside) < org[3].dist < (out + inside)):
                img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                                (0, 255, 255), 1)
                to_classify = sort_by_dist(curr_point, to_classify)
                for i in range(4):
                    if (len(to_classify) > 0):
                        next_dot = to_classify.pop(0)
                        img_with_keypoints = cv2.circle(img_with_keypoints, (int(next_dot.x), int(next_dot.y)),
                                                        int(out),
                                                        (0, 255, 255), 1)
                cv2.putText(img=img_with_keypoints, text=str(5), org=(int(curr_point.x), int(curr_point.y)),
                            fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

            # 3len > x (3)
            elif (len(org) > 3 and org[3].dist > (out + inside)):
                img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                                (0, 0, 255), 1)
                to_classify = sort_by_dist(curr_point, to_classify)
                for i in range(2):
                    if (len(to_classify) > 0):
                        next_dot = to_classify.pop(0)
                        img_with_keypoints = cv2.circle(img_with_keypoints, (int(next_dot.x), int(next_dot.y)),
                                                        int(out),
                                                        (0, 0, 255), 1)
                cv2.putText(img=img_with_keypoints, text=str(3), org=(int(curr_point.x), int(curr_point.y)),
                            fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

        # 1len = 2x/sqrt(2) (4)
        elif (len(org) > 3 and 2 * (out - inside) / math.sqrt(2) < org[1].dist < 2 * (out + inside) / math.sqrt(2)):
            img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                            (255, 255, 255), 1)
            to_classify = sort_by_dist(curr_point, to_classify)
            for i in range(3):
                if (len(to_classify) == 0): break
                next_dot = to_classify.pop(0)
                img_with_keypoints = cv2.circle(img_with_keypoints, (int(next_dot.x), int(next_dot.y)), int(out),
                                                (255, 255, 255), 1)
            cv2.putText(img=img_with_keypoints, text=str(4), org=(int(curr_point.x), int(curr_point.y)),
                        fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

        # 1len = x/sqrt(2) (6)
        elif (len(org) > 5 and (out - inside) / math.sqrt(2) < org[1].dist < (out + inside) / math.sqrt(2)):
            img_with_keypoints = cv2.circle(img_with_keypoints, (int(curr_point.x), int(curr_point.y)), int(out),
                                            (255, 0, 255),
                                            1)
            to_classify = sort_by_dist(curr_point, to_classify)
            for i in range(5):
                if (len(to_classify) == 0): break
                next_dot = to_classify.pop(0)
                img_with_keypoints = cv2.circle(img_with_keypoints, (int(next_dot.x), int(next_dot.y)), int(out),
                                                (255, 0, 255), 1)
            cv2.putText(img=img_with_keypoints, text=str(6), org=(int(curr_point.x), int(curr_point.y)),
                        fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=3, color=(1, 1, 255), thickness=4)

        # when dot was not classified (side dots of 3 and 5)
        else:
            to_classify.insert(0, curr_point)
        iter = iter + 1


while True:

    ret, im = cap.read()

    # path = os.path.abspath("resources/dices2.jpg")
    # im = cv2.imread(path)

    params = cv2.SimpleBlobDetector_Params()
    params.filterByArea = True
    params.filterByColor = color
    params.filterByCircularity = True
    params.filterByInertia = True
    params.minThreshold = min_threshold
    params.maxThreshold = max_threshold
    params.minArea = min_area
    params.minCircularity = min_circularity
    params.minInertiaRatio = min_inertia_ratio
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

    detector = cv2.SimpleBlobDetector_create(params)  # create a blob detector object.

    keypoints = detector.detect(im)  # keypoints conatains coordinates of centers and diameters of circles

    distance = []

    reading = len(keypoints)  # number of circles

    total = "Number of dots: " + str(len(keypoints))
    cv2.putText(img=im,
                text=total,
                org=(10, 50),
                fontFace=cv2.FONT_HERSHEY_PLAIN,
                fontScale=3,
                color=(1, 1, 255),
                thickness=4)

    im_with_keypoints = cv2.drawKeypoints(im, keypoints, np.array([]), (0, 0, 255),
                                          cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    counter(im_with_keypoints, keypoints)
    cv2.imshow("Dice paradise", im_with_keypoints)  # display the frame with keypoints added.

    k = cv2.waitKey(30) & 0xff  # press [Esc] to exit.
    if k == 27:
        break

cv2.destroyAllWindows()
